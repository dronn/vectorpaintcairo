#ifndef LOADOLD_H
#define LOADOLD_H

#include <gtk/gtk.h>
#include <cairo.h>
#include <math.h>
#include <gdk/gdkkeysyms.h>
#include <string.h>
#include <glib.h>
#include <stdint.h>
#include <malloc.h>
#include <locale.h> 
#include <stdlib.h>		

uint16_t load_old(const char* sketch, const char* rendered_sketch, uint16_t** data);

double guess_variance(double stroke, double* rendered_strokes);
uint16_t guess_fingers(char* path, char* rendered_pathes);

void colour_from_string(char* string, uint16_t** colour);

uint8_t is_svg( char* string);


uint16_t get_path_size( char * pathString);

void path_from_string( char* string, uint16_t** path, uint16_t offset, uint16_t path_len);

uint16_t loadFile2string(const char* filename, char** string);

void get_stroke_properties(char* svg_string, uint16_t** header, uint16_t offset);

uint16_t get_stroke_num(char* svg_string);

uint16_t count_substring(char* sring, const char* substring);

#endif
