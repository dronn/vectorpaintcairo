#include "colour_harmony_variance.h"




void rgb2hsv(double r, double g, double b, double* h, double* s, double* v)
{
   
    double      min, max, delta;

    min = r < g ? r : g;
    min = min  < b ? min  : b;

    max = r > g ? r : g;
    max = max  > b ? max  : b;

    *v = max;                                // v
    delta = max - min;
    if (delta < 0.00001)
    {
        s = 0;
        h = 0; // undefined, maybe nan?
        return;
    }
    if( max > 0.0 ) { // NOTE: if Max is == 0, this divide would cause a crash
        *s = (delta / max);                  // s
    } else {
        // if max is 0, then r = g = b = 0              
        // s = 0, h is undefined
        *s = 0.0;
        *h = 0.0;                            // its now undefined
        return ;
    }
    if( r >= max )                           // > is bogus, just keeps compilor happy
        *h = ( g - b ) / delta;        // between yellow & magenta
    else
    if( g >= max )
        *h = 2.0 + ( b - r ) / delta;  // between cyan & yellow
    else
        *h = 4.0 + ( r - g ) / delta;  // between magenta & cyan

    *h *= 60.0;                              // degrees

    if( *h < 0.0 )
        *h += 360.0;

    return ;
}


void hsv2rgb(double h , double s, double v, double* r, double* g, double* b)
{
    double      hh, p, q, t, ff;
    int        i;


    if(s <= 0.0) {       // < is bogus, just shuts up warnings
        *r = v;
        *g = v;
        *b = v;
        return ;
    }
    hh = h;
    if(hh >= 360.0) hh = 0.0;
    hh /= 60.0;
    i = (int)hh;
    ff = hh - i;
    p = v * (1.0 -  s);
    q = v * (1.0 - (s * ff));
    t = v * (1.0 - (s * (1.0 - ff)));

    switch(i) {
    case 0:
        *r = v;
        *g = t;
        *b = p;
        break;
    case 1:
        *r = q;
        *g = v;
        *b = p;
        break;
    case 2:
        *r = p;
        *g = v;
        *b = t;
        break;

    case 3:
        *r = p;
        *g = q;
        *b = v;
        break;
    case 4:
        *r = t;
        *g = p;
        *b = v;
        break;
    case 5:
    default:
        *r = v;
        *g = p;
        *b = q;
        break;
    }
    return;     
}

void get_harmony_analogue(int index, gdouble* h)
{
	/*
    formula: H1 = |(H0 + 30 degrees) - 360 degrees|
    formula: H2 = |(H0 + 60 degrees) - 360 degrees|
    formula: H3 = |(H0 + 90 degrees) - 360 degrees|
    */
    
    if(index%3==0)
    {
		*h=fabs((*h+30)-360);
	}
	else if(index%3==1)
	{
		*h=fabs((*h+60)-360);
	}
	else if(index%3==2)
	{
		*h=fabs((*h+90)-360);
	}

}
void get_harmony_complement(int index, gdouble* h)
{
		*h=fabs((*h+180)-360);
}

void get_harmony_triadic(int index, gdouble* h)
{
	/*
    formula: H1 = |(H0 + 120 degrees) - 360 degrees|
    formula: H2 = |(H0 + 240 degrees) - 360 degrees|
    */
        if(index%3==0)
    {
		
	}
	else if(index%3==1)
	{
		*h=fabs((*h+120)-360);
	}
	else if(index%3==2)
	{
		*h=fabs((*h+240)-360);
	}

}
void get_harmony_tedradic(int index, gdouble* h)
{
	/*
    formula: H1 = |(H0 + 90 degrees) - 360 degrees|
    formula: H2 = |(H0 + 180 degrees) - 360 degrees|
    formula: H3 = |(H0 + 270 degrees) - 360 degrees|
    */
    if(index%4==3)
    {
		*h=fabs((*h+30)-360);
	}
	else if(index%4==1)
	{
		*h=fabs((*h+60)-360);
	}
	else if(index%4==2)
	{
		*h=fabs((*h+90)-360);
	}

}

void apply_colour_variance(int index, int fingers, gdouble variance, gdouble* r, gdouble* g, gdouble* b)
{
	gdouble in_r, in_g, in_b;
	gdouble out_r, out_g, out_b;
	gdouble in_h, in_s, in_v;
		
	in_r=(*r*1.0)/ 65535.0;
	in_g=(*g*1.0)/ 65535.0;
	in_b=(*b*1.0)/ 65535.0;
	
	if(variance==0.0)
	{
		*r=in_r*65535.0;
		*g=in_g*65535.0;
		*b=in_b*65535.0;
		return;
	}
	
	rgb2hsv(in_r, in_g, in_b, &in_h, &in_s, &in_v);
	
	int random_variable=rand()%10;
	if(fingers< 4 || variance <=0.2 || random_variable <2)
	{
		int random_variable2=rand()%20;
		in_h+=fabs(random_variable2-360);
		
	}
	if(fingers< 4 || variance <=0.4 || random_variable <4)
	{
			get_harmony_analogue(index, &in_h);
			//printf("colour harmonic analogue %f\n", variance);
	}
	else if(fingers<7|| variance < 0.6|| random_variable >4 && random_variable<8)
	{
			get_harmony_triadic(index, &in_h);
				//printf("colour harmonic triadic %f\n", variance);

	}
	else if(variance > 0.7 || random_variable > 8)
	{
		get_harmony_complement(index, &in_h);
				//printf("colour harmonic complement %f\n", variance);

	}
	else //if(variance > 0.7||fingers>6)
	{
		get_harmony_tedradic(index, &in_h);
				//	printf("colour harmonic tedradic %f\n", variance);

	}
	
	hsv2rgb(in_h, in_s, in_v, &out_r, &out_g, &out_b);
	
	*r=out_r*65535.0;
	*g=out_g*65535.0;
	*b=out_b*65535.0;

}
		
void apply_variance(int index, int fingers, gdouble variance,
						gdouble* pen_radius, GdkColor* color, gdouble* alpha)
{
	if(variance == 0.0)
		return;
	
	int random_variable=rand()%10;
	if(random_variable>10*variance)
		return;
	else
	{
		int random_variable2=rand()%10;
		int random_variable3=rand()%10;
		*pen_radius=*pen_radius *((random_variable2*4.0)/9.0);
		*alpha=*alpha*((random_variable3*1.0)/9.0+0.1);
		gdouble r, g, b;
		r=color->red;
		g=color->green;
		b=color->blue;
		apply_colour_variance(index, fingers, variance, &r, &g, &b);
		color->red=r;
		color->green=g;
		color->blue=b;
	}
}
