
#ifndef GRAPHICS_OBJECT_H
#define GRAPHICS_OBJECT_H

#include <gtk/gtk.h>
#include <cairo.h>
#include <math.h>
#include <gdk/gdkkeysyms.h>
#include <string.h>
#include <glib.h>
#include <stdint.h>
#include <malloc.h>
#include <locale.h> 
#include <stdlib.h>		

//#define PANDORA 

#ifdef PANDORA
#define WIDTH  800
#define HEIGHT 480
#else
#define WIDTH  1920
#define HEIGHT 1080
#endif

enum vectorObjectIdentifier
{PEN, LOOP};

/*
 * object header containing type, radius, color and alpha of the graphic object
*/
struct graphicObject
{

	enum vectorObjectIdentifier type;

	double pen_radius;
	gint fingers;
	gdouble variance;

	GdkColor color;
	double alpha;
};

// struct coordinate
struct coord
{
	double x;
	double y;
};

#endif
