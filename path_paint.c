//todo:

// timeline

//morph single path to target

//morph multiple paths to/from target

//boolean operations

//smooth

//grouping

//linking
	//linked brush

//file size estimation

//rendertime estimation


#include <gtk/gtk.h>
#include <cairo.h>
#include <math.h>
#include <gdk/gdkkeysyms.h>
#include <string.h>
#include <glib.h>
#include <stdint.h>
#include <malloc.h>
#include <locale.h> 
#include "colour_harmony_variance.h"
#include "binary_format.h"
#include "graphics_object.h"
#include "morph.h"
#include <time.h>
#include <loadOld.h>




#define STRIDE WIDTH*4

/* uncomment this to decrease the density of coordinate samples used for
 * the current stroke, this is not an optimal solution, a better solution
 * would be to use actual smoothing on the coordinate data
 */
#define MAX_COORDS 1024


#define USE_HINT

#define HAS_NANOSVG 1

#if HAS_NANOSVG
#include <stdio.h>
#include <string.h>
#include <float.h>
//#define STB_IMAGE_WRITE_IMPLEMENTATION
//#include "stb_image_write.h"
#define NANOSVG_IMPLEMENTATION
#include "nanosvg.h"
#define NANOSVGRAST_IMPLEMENTATION
#include "nanosvgrast.h"

#endif


GtkWidget *canvas = NULL;

/* contains a list of doubly linked lists
 * lists contain graphicObject struct as "header"
 * followd by coordinates
 * */
GList* history;
GList* loaded_history;
GList* rendered;

// pointer to latest objects header and coordinate list
GList* latestObj;
GList** latestRenderedObj;







//moved to graphics object header


//enum vectorObjectIdentifier
//{PEN, LOOP};

/*
 * object header containing type, radius, color and alpha of the graphic object
*/



//struct graphicObject
//{
//
//	enum vectorObjectIdentifier type;
//
//	double pen_radius;
//	gint fingers;
//	gdouble variance;
//
//	GdkColor color;
//	double alpha;
//};

// struct coordinate
//struct coord
//{
//	double x;
//	double y;
//};

uint16_t seeed=1227;

static GtkWidget *da;
static GdkColor color;
static GdkColor cHistory[20];
gint cHisIdx=0;
double alpha =0.5;

uint16_t* binary_data, *binary_data_to, *binary_data_target;

gboolean pen_color_is_black = TRUE;
double     pen_radius         = 0.5;
gint     loop_mode = 1;
gint fingers=1;
gint fingers_old=1;
gdouble variance=0.0;


gdouble coord_x [MAX_COORDS];
gdouble coord_y [MAX_COORDS];
gint    coord_count = 0;

uint16_t historyIdx=0;
uint16_t coordsIdx=0;
uint8_t flipNormals=0;

guchar buffer      [WIDTH*HEIGHT*4];
guchar temp_buffer [WIDTH*HEIGHT*4];

cairo_surface_t *back_surface = NULL;
cairo_surface_t *temp_surface = NULL;


cairo_t         *cr_save    = NULL;

unsigned char* img = NULL;
unsigned char* img2 = NULL;

int loaded_w, loaded_h;

gboolean pen_is_down = FALSE;

/* utility functions, defined at bottom,
 * constructing a path from coordinate arrays
 */
static void
points_to_linear_path (cairo_t *cr,
                       gdouble  coord_x[],
                       gdouble  coord_y[],
                       gint     n_coords);
static void
points_to_bezier_path (cairo_t *cr,
                       gdouble  coord_x[],
                       gdouble  coord_y[],
                       gint     n_coords);

static void
points_to_linear_path_fingers_list (cairo_t *cr,
									gint coord_count);

// helperfunction to convert 16bit per channel to 8 bit per channel hex string
uint8_t hexOfFirst(uint8_t val)
{

	if(val > 15) return 'i';//invalid
	if(val < 10) return val+'0';
	else return val+ 'a'-10;
}

// returns char[8] string containing #rrggbb hex color
// remember to free da ding
char* gdk_color_to_string6(GdkColor* color)
{
	char* tmp=(char*)malloc(sizeof(char)*8);
	uint8_t r= color->red/256;
	uint8_t g= color->green/256;
	uint8_t b= color->blue/256;
	tmp[0]='#';

	tmp[2]=hexOfFirst(r&0xf);
	tmp[1]=hexOfFirst((r>>4)&0xf);
	tmp[4]=hexOfFirst(g&0xf);
	tmp[3]=hexOfFirst((g>>4)&0xf);
	tmp[6]=hexOfFirst(b&0xf);
	tmp[5]=hexOfFirst((b>>4)&0xf);
	tmp[7]='\0';

	return tmp;
}


void print_coord(struct coord* coo)
{
	
	printf("%d,%d ", (int)coo->x, (int)coo->y);
	
}
void fprint_coord(FILE* file, struct coord* coo)
{
	
	fprintf(file, "%d,%d ", (int)coo->x, (int)coo->y);
}

//todo add interpolated points
/*
 * write svg to file
 */
void save_svg(uint8_t save_rendered, char* filename)
{
	setlocale (LC_ALL,"C");
	FILE *file;
	//if(save_rendered)
	//	file=fopen("./tst_r.svg", "w");
	//else
		file=fopen(filename, "w");
	
	char* svgHeader=
"<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>\n"
"<svg\n"
"   xmlns:dc=\"http://purl.org/dc/elements/1.1/\"\n"
"   xmlns:cc=\"http://creativecommons.org/ns#\"\n"
"   xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"\n"
"   xmlns:svg=\"http://www.w3.org/2000/svg\"\n"
"   xmlns=\"http://www.w3.org/2000/svg\"\n"
#ifdef PANDORA
"   viewBox=\"0 0 800 480\"\n"
"   version=\"1.1\"\n"
"   id=\"svg2\"\n"
"   height=\"480\"\n"
"   width=\"800\">\n"
#else
"   viewBox=\"0 0 1920 1080\"\n"
"   version=\"1.1\"\n"
"   id=\"svg2\"\n"
"   height=\"1080\"\n"
"   width=\"1920\">\n"
#endif
"  <defs"
"     id=\"defs4\" />\n"
"  <metadata\n"
"     id=\"metadata7\">\n"
"    <rdf:RDF>\n"
"      <cc:Work\n"
"         rdf:about=\"\">\n"
"        <dc:format>image/svg+xml</dc:format>\n"
"        <dc:type\n"
"           rdf:resource=\"http://purl.org/dc/dcmitype/StillImage\" />\n"
"        <dc:title></dc:title>\n"
"      </cc:Work>\n"
"    </rdf:RDF>\n"
"  </metadata>\n"
"  <g\n"
"     id=\"layer1\" >\n";

		GList* firstHistory= NULL;
		GList* pHistory= NULL;
		GList* lastHistory= NULL;

	if(save_rendered){
		 firstHistory= g_list_first(rendered);
		pHistory= g_list_first(rendered);
		lastHistory= g_list_last(rendered);
		}
		else
		{
			
		 firstHistory= g_list_first(history);
		 pHistory= g_list_first(history);
		 lastHistory= g_list_last(history);
		
		}
	
	GList* firstCoord;
	GList* pCoord;
	GList* lastCoord;
	fprintf(file, "%s",svgHeader);

	int i=0;
	for(;pHistory!=lastHistory;pHistory=pHistory->next, i++)
	{
		firstCoord=(GList*)pHistory->data;
		pCoord=firstCoord->next;
		
		lastCoord=g_list_last(firstCoord);
			if(pCoord){
			
			fprintf(file, "<path\n       id=\"path%i\"\n       d=\"M %d, %d L ",i,(int)(((struct coord*)pCoord->data)->x),(int)(((struct coord*)pCoord->data)->y) );   
			struct graphicObject* header =(struct graphicObject*)firstCoord->data;
			
			for(;pCoord!=lastCoord;pCoord=pCoord->next)
			{
				fprint_coord(file, (struct coord*)pCoord->data);

			}
			fprint_coord(file, (struct coord*)pCoord->data);

			
			fprintf(file,"\"\n");
			if(header->type==LOOP)
				fprintf(file, "style=\"opacity: %f ;fill:%s;fill-opacity:1;stroke:none;stroke-width: %f ;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1\" />\n", header->alpha, gdk_color_to_string6(&header->color), (header->pen_radius*2.0));
			else
				fprintf(file, "style=\"opacity: %f ;fill:none;fill-opacity:1;stroke:%s;stroke-width: %f ;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1\" />\n", header->alpha, gdk_color_to_string6(&header->color), (header->pen_radius*2.0));
		}
	}
	
	
	firstCoord=(GList*)pHistory->data;
	pCoord=firstCoord->next;
	lastCoord=g_list_last(firstCoord);
	
	fprintf(file, "<path\n       id=\"path%i\"\n       d=\"M %d, %d L ",i,(int)(((struct coord*)pCoord->data)->x),(int)(((struct coord*)pCoord->data)->y) );      
	struct graphicObject* header =(struct graphicObject*)firstCoord->data;

	for(;pCoord!=lastCoord;pCoord=pCoord->next)
	{
		fprint_coord(file, (struct coord*)pCoord->data);

	}
	fprint_coord(file, (struct coord*)pCoord->data);
	
	fprintf(file, "\"\n");
	
	if(header->type==LOOP)
		fprintf(file, "style=\"opacity: %f ;fill:%s;fill-opacity:1;stroke:none;stroke-width: %f ;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1\" />\n", header->alpha, gdk_color_to_string6(&header->color), (header->pen_radius*2.0));
	else
		fprintf(file, "style=\"opacity: %f ;fill:none;fill-opacity:1;stroke:%s;stroke-width: %f ;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1\" />\n", header->alpha, gdk_color_to_string6(&header->color), (header->pen_radius*2.0));

 fprintf(file, " </g> </svg>\n");
 
 fclose(file);
	

}

int doNanoSVG(const char* filename)
{
	#if HAS_NANOSVG
	NSVGimage *image = NULL;
	NSVGrasterizer *rast = NULL;

	int w, h;
	//const char* filename = "../example/23.svg";

	printf("parsing %s\n", filename);
	image = nsvgParseFromFile(filename, "px", 96.0f);
	if (image == NULL) {
		printf("Could not open SVG image.\n");
		goto error;
	}
	w = (int)image->width;
	h = (int)image->height;

	rast = nsvgCreateRasterizer();
	if (rast == NULL) {
		printf("Could not init rasterizer.\n");
		goto error;
	}

	img = malloc(w*h*4);
	img2 = malloc(w*h*4);
	
	if (img == NULL) {
		printf("Could not alloc image buffer.\n");
		goto error;
	}
	if (img2 == NULL) {
		printf("Could not alloc image buffer.\n");
		goto error;
	}

	printf("rasterizing image %d x %d\n", w, h);
	nsvgRasterize(rast, image, 0,0,1, img, w, h, w*4);
	
	for(int i=0; i< w*h*4; i+=4)
	{
		img2[i]=img[i+2];
		img2[i+1]=img[i+1];
		img2[i+2]=img[i];
		img2[i+3]=img[i+3];
				
	}
	//stbi_write_png("svg.png", w, h, 4, img, w*4);	
	//stbi_write_png("svg2.png", w, h, 4, img2, w*4);
	loaded_w=w;
	loaded_h=h;
	
	error:
	nsvgDeleteRasterizer(rast);
	nsvgDelete(image);
		#endif
	
}
static void render_history();

static void
pen_down (gdouble  x,
            gdouble  y);
            
            static void
pen_motion (gdouble  x,
            gdouble  y);
            
            
            static void
pen_up(gdouble  x,
            gdouble  y);

int loadOld(const char* sketch, const char* rendered_sketch)
{            

	uint16_t error=load_old(sketch, rendered_sketch, &binary_data);
	//save_bin("old", binary_data);
	if(!error){
	populate_history(binary_data, &seeed, &loaded_history);
	
		

	render_history();
	
	}


}
static int loadSVG_morph_batched(const char* from, const char* to, double stage, const char* out)
{
	char* out_b=(char*)malloc(strlen(out)+3);
	char* out_r=(char*)malloc(strlen(out)+7);
	char* out_p=(char*)malloc(strlen(out)+7);
	sprintf(out_b, "%s_b", out);
	sprintf(out_r, "%s_r.svg", out);
	sprintf(out_p, "%s_p.png", out);
	
	load_bin(from, &binary_data);
	printf("files: %s %s\n", from, to);
	
	load_bin(to, &binary_data_to);
	printf("pointer: %d %d\n", binary_data, binary_data_to);
	
	morph(binary_data, binary_data_to, &binary_data_target, stage);
	
	populate_history(binary_data_target, &seeed, &loaded_history);
	render_history();
	
	save_bin(out_b, binary_data_target);
	save_svg(1, out_r);
	save_svg(0, out);
	save_png(out_p);


}


int loadSVG_morph(const char* from, const char* to, double stage)
{
	load_bin(from, &binary_data);
	printf("files: %s %s\n", from, to);
	
	load_bin(to, &binary_data_to);
	printf("pointer: %d %d\n", binary_data, binary_data_to);
	
	morph(binary_data, binary_data_to, &binary_data_target, stage);
	save_bin("morphed_b", binary_data_target);
	populate_history(binary_data_target, &seeed, &loaded_history);
	render_history();
}
            
int loadSVG(const char* filename)
{
	char key[] = ".svg";
	
	if(strstr (filename, key) == NULL){


	//load_binary

	printf("load binary\n");
	load_bin(filename, &binary_data);
	//print_sketch_debug(&binary_data);
	printf("data loaded(%i): %i strokes %i data size\n",&binary_data,binary_data[1], binary_data[2]  );
	//history=g_list_alloc();
	populate_history(binary_data, &seeed, &loaded_history);
	
		

	render_history();
	
} 
else
{
printf("load svg\n");
#if HAS_NANOSVG
	int w, h;
	w=loaded_w;
	h=loaded_h;

	cairo_surface_t* surf= cairo_image_surface_create_for_data (img2,CAIRO_FORMAT_ARGB32,w,h,w*4);

	cairo_surface_write_to_png(surf,"./outus.png");

	cairo_set_source_surface (cr_save, surf, 0, 0);
	cairo_paint (cr_save);

	memcpy (buffer, temp_buffer, HEIGHT*STRIDE);

	cairo_surface_destroy (surf);
	free(img);
	free(img2);



#endif

}
	
return 0;
}

void save_png(char* filename)
{
	cairo_surface_write_to_png(temp_surface, filename);

}

static void
drawapp_render (cairo_t *cr)
{

  cairo_save (cr);

  cairo_set_source_rgb (cr, 1,1,1);
  cairo_paint (cr);

  cairo_set_source_surface (cr, back_surface, 0, 0);
  cairo_paint (cr);
	if(loop_mode){ 
  cairo_set_line_width (cr, pen_radius*2);

  cairo_set_source_rgba (cr, (color.red*1.0)/ 65535.0,(color.green*1.0)/ 65535.0,(color.blue*1.0)/ 65535.0, alpha);

  cairo_set_line_join (cr, CAIRO_LINE_JOIN_ROUND);
  cairo_set_line_cap (cr, CAIRO_LINE_CAP_ROUND);
}
  if(loop_mode) points_to_bezier_path (cr, coord_x, coord_y, coord_count);
  //else points_to_linear_path (cr, coord_x, coord_y, coord_count);
  else points_to_linear_path_fingers_list (cr, coord_count);
 if(loop_mode)
 { 
  cairo_stroke (cr);

 
}
 cairo_restore (cr);
}

static void
apply_coords (guchar *buffer)
{

  drawapp_render (cr_save);
  memcpy (buffer, temp_buffer, HEIGHT*STRIDE);
}

static void
coords_clear (void)
{
  coord_count = 0;
}
static void calc_normal_1(gdouble dx_, gdouble dy_, gdouble pen_size, gdouble* nx, gdouble* ny)
{
	gdouble dx=dx_;
	gdouble dy=dy_;
	
		if(fabs(dx)>pen_radius||fabs(dy)>pen_radius)
		{
			if(fabs(dx)> fabs(dy))
			{
				
				double old=dx;
				dx=pen_radius;
				dy=pen_radius*dy/old;
			}
			else
			{
				double old=dy;
				dy=pen_radius;
				dx=pen_radius*dx/old;
			}
		}else if(fabs(dx)<pen_radius||fabs(dy)<pen_radius&& dx!=0.0 && dy!=0.0 )
		{
			if(fabs(dx)> fabs(dy))
			{
				
				double old=dx;
				if(old> 0.001)
				{
				dx=pen_radius;
				dy=pen_radius*dy/old;}
			}
			else
			{
				double old=dy;	if(old> 0.001)
				{
				dy=pen_radius;
				dx=pen_radius*dx/old;
				}
			}
		}
		//dx=x2-x1 and dy=y2-y1, then the normals are (-dy, dx) and (dy, -dx).
		*nx=-dy*pen_radius;
		*ny=dx*pen_radius;
		
}
static void calc_normal_2(gdouble dx_, gdouble dy_, gdouble pen_size, gdouble* nx, gdouble* ny)
{
	gdouble dx=dx_;
	gdouble dy=dy_;

		if(fabs(dx)>pen_radius||fabs(dy)>pen_radius)
		{
			if(fabs(dx)> fabs(dy))
			{
				
				double old=dx;
				dx=pen_radius;
				dy=pen_radius*dy/old;
			}
			else
			{
				double old=dy;
				dy=pen_radius;
				dx=pen_radius*dx/old;
			}
		}else if(fabs(dx)<pen_radius||fabs(dy)<pen_radius&& dx!=0.0 && dy!=0.0 )
		{
			if(fabs(dx)> fabs(dy))
			{
				
				double old=dx;
				if(old> 0.001)
				{
				dx=pen_radius;
				dy=pen_radius*dy/old;}
			}
			else
			{
				double old=dy;	if(old> 0.001)
				{
				dy=pen_radius;
				dx=pen_radius*dx/old;
				}
			}
		}
		//dx=x2-x1 and dy=y2-y1, then the normals are (-dy, dx) and (dy, -dx).
		*nx=dy*pen_radius;
		*ny=-dx*pen_radius;
		
}


static double derivate3(gdouble x1, gdouble x2, gdouble x3, gdouble y1, gdouble y2, gdouble y3 )
{
	//printf("turning %f?\n", (2*y1/((x2-x1)*(x3-x1))*(-2)*y2/((x3-x2)*(x2-x1))*(-2)*y3/(x3-x2)*(x3-x1)));
	return (2*y1/((x2-x1)*(x3-x1))*(-2)*y2/((x3-x2)*(x2-x1))*(-2)*y3/(x3-x2)*(x3-x1));
	
}

static uint8_t is_turningpoint(gdouble x1, gdouble x2, gdouble x3, gdouble y1, gdouble y2, gdouble y3)
{
	
	double derv3=derivate3( x1,  x2,  x3,  y1,  y2,  y3 );
	if(isnan(derv3)||isinf(derv3))
		return 1;
	else return 0;
}
static uint8_t signchangeButNotinAlternatertive(gdouble nx,gdouble ny, gdouble nx_1, gdouble ny_1, gdouble nx_1_, gdouble ny_1_)
{
	uint8_t changeX, changeY, change2, returnValue;
	returnValue=0;
	changeX=0;
	changeY=0;
	change2=0;
	
	if(nx< 0.0 && nx_1 > 0.0|| nx > 0.0 && nx_1 < 0.0)
	{
		changeX=1;
	}
	if(ny<0.0 && ny_1 > 0.0 || ny > 0.0 && ny_1 < 0.0)
	{
		changeY=1;
	}
	
	if(!changeX && !changeY)return 0;
	if(nx< 0.0 && nx_1_ > 0.0|| nx > 0.0 && nx_1_ < 0.0)
	{
		change2=1;
		return 0;
	}
		if(ny< 0.0 && ny_1_ > 0.0 || ny > 0.0 && ny_1_ < 0.0)
	{
		change2=1;
		return 0;
	}
	
	if((changeX||changeY)&&(!change2))return 1;
	else return 0;
	
}

static gdouble det(gdouble x1, gdouble y1, gdouble x2, gdouble y2)
{
	return x1*y2-y1*x2;
}
    

static uint8_t intersection_exists(gdouble x1, gdouble y1, gdouble x2, gdouble y2,
							gdouble x3, gdouble y3, gdouble x4, gdouble y4 )
{

	gdouble xdiff1=x1-x2;
	gdouble xdiff2=x3-x4;
	gdouble ydiff1=y1-y2;
	gdouble ydiff2=y3-y4;


	gdouble div = det( xdiff1, xdiff2, ydiff1, ydiff2);

	if(div==0.0)return 0;



	 gdouble  d1 = det(x1,y1,x2,y2);
	 gdouble  d2 = det(x3,y3,x4,y4);
	 gdouble   x = det(d1, d2, xdiff1, xdiff2) / div;
	 gdouble   y = det(d1, d2, ydiff1, ydiff2) / div;
	 
	// printf("x %f y %f, %f %f %f %f \n", x, y, x1, x4, y1, y4);
	 
	 uint8_t withinx=0;
	 uint8_t withiny=0;
	 
	 
	if(x > x1 && x < x4)withinx=1;
	if(y > y1 && y < y4)withiny=1;

	if(x > x4 && x < x1) withinx=1;
	if(y > y4 && y < y1) withinx=1;



	 return withinx||withiny;

}



static void fingers_add_coord(gdouble x,
			gdouble y)
{
	for(int i=0; i<fingers; i++)
	{
	  struct coord *newCoord= (struct coord*) malloc(sizeof(struct coord));
		double x_offset=0.0;
		double y_offset=0.0;
		double old_x=0.0;
		double old_y=0.0;
		//dx=x2-x1 and dy=y2-y1, then the normals are (-dy, dx) and (dy, -dx).
		if(i>0)
		{
		old_x=((struct coord*)(latestObj->data))->x;
		old_y=((struct coord*)(latestObj->data))->y;
		double dx=x-old_x;
		double dy=y-old_y;
		
		

	
	

		gdouble nx, ny;
		gdouble nx_, ny_;
	
			calc_normal_1(dx, dy, pen_radius, &nx, &ny);

			calc_normal_2(dx, dy, pen_radius, &nx_, &ny_);
		
	if( coord_count> 4 )
		{
			double x_1=0.0;
			double y_1=0.0;
			double x_2=0.0;
			double y_2=0.0;
			double dx_1=0.0;
			double dy_1=0.0;
			double dx_2=0.0;
			double dy_2=0.0;
			
			GList* prev=g_list_last(latestRenderedObj[0]);
			prev=g_list_previous(prev);
			if(prev!=NULL)
			{
				x_1=((struct coord*)prev->data)->x;
				y_1=((struct coord*)prev->data)->y;
			}
			prev=g_list_previous(prev);
			if(prev!=NULL)
			{
			x_2=((struct coord*)prev->data)->x;
			y_2=((struct coord*)prev->data)->y;
			}
			dx_1=old_x-x_2;
			dx_2=x_1-x_2;
			
			dy_1=old_y-y_2;
			dy_2=y_1-y_2;
			//normals variant1
			gdouble nx_1, nx_2, ny_1, ny_2;
			//normals variant2
			gdouble nx_1_, nx_2_, ny_1_, ny_2_;
			calc_normal_1(dx_1, dy_1, pen_radius, &nx_1, &ny_1);
			calc_normal_1(dx_2, dy_2, pen_radius, &nx_2, &ny_2);
			
			calc_normal_2(dx_1, dy_1, pen_radius, &nx_1_, &ny_1_);
			calc_normal_2(dx_2, dy_2, pen_radius, &nx_2_, &ny_2_);
			
			x_offset=nx*i;
			y_offset=ny*i;
		
		

				
				gdouble old3_x=old_x+i*nx_1;
				gdouble old3_y=old_y+i*ny_1;
				gdouble current3_x=x+nx*i;
				gdouble current3_y=y+ny*i;
				
				uint8_t intersect=intersection_exists(old_x, old_y, x, y, old3_x, old3_y, current3_x, current3_y);
				
				if(intersect)
					flipNormals=!flipNormals;


			
			
				
		}
		if((i+flipNormals)%2==0)
		{
			x_offset=nx_*i;
			y_offset=ny_*i;
		}
		else
		{
			x_offset=nx*i;
			y_offset=ny*i;
		}
		
		
		
			}
	  newCoord->x=x+x_offset;
      newCoord->y=y+y_offset;
      //if(i==2)printf("%f, %f; %f\n",x_offset, y_offset, pen_radius );
      latestRenderedObj[i]=g_list_append(latestRenderedObj[i],newCoord);
      latestRenderedObj[i] =g_list_last(latestRenderedObj[i]);

	
}
}
			
static void finalise_stroke()
{
	if(!loop_mode){
		if(coord_count> 3 && fingers > 1)
		{
			for(int i=0; i< fingers; i++)
			{
				
				GList* last=g_list_last(latestRenderedObj[i]);
				GList* prev=g_list_previous(latestRenderedObj[i]);
				
				
				gdouble x0=((struct coord*)prev->data)->x;
				gdouble y0=((struct coord*)prev->data)->y;
				
				gdouble x1=((struct coord*)last->data)->x;
				gdouble y1=((struct coord*)last->data)->y;
				gdouble fact=((rand()%10)*1.0)/10.0;
				gdouble dx=(x1-x0)*fact;
				gdouble dy=(y1-y0)*fact;
				((struct coord*)last->data)->x=x0+dx;
				((struct coord*)last->data)->y=y0+dy;
			}
		}
	}
}
static void
coords_add (gdouble x,
            gdouble y)
{
  if (coord_count< MAX_COORDS-3)
    {
      coord_x [coord_count]=x;
      coord_y [coord_count]=y;
      struct coord *newCoord= (struct coord*) malloc(sizeof(struct coord));
      newCoord->x=x;
      newCoord->y=y;
      //GList* tmp= g_list_last(history);
	if(    (fabs((x - coord_x [coord_count-1]) *(y - coord_y [coord_count-1])) > 2.0) || coord_count<=1 )
      	{
		  if(coord_count>=1)
			fingers_add_coord(x,y);
		
		  latestObj =g_list_append(latestObj,newCoord);
		  latestObj =g_list_last(latestObj);
		  
		coord_count++;
	}
    }
}

static void render_history()
{
	
if(!loaded_history)return;
GList* currentHistoryp=g_list_first(loaded_history);
GList* currentObject=((GList*)currentHistoryp->data);
GList* lastObjectInHistory=g_list_last(loaded_history);

for(;currentHistoryp!=lastObjectInHistory;currentHistoryp=g_list_next(currentHistoryp))
{
	currentObject=((GList*)currentHistoryp->data);
	currentObject=g_list_next(currentObject);

	struct graphicObject* newVGO= ((struct graphicObject*)currentObject->data);
	loop_mode=newVGO->type;
	fingers=newVGO->fingers;
	variance=newVGO->variance;
	pen_radius=newVGO->pen_radius;
	color=newVGO->color;
	alpha=newVGO->alpha;
	


	   GList* currentCoord=g_list_next(currentObject);
	   GList* lastCoord=g_list_last(currentCoord);
	   if(currentCoord!=NULL&&lastCoord!=NULL){
		   pen_down(((struct coord*)currentCoord->data)->x,((struct coord*)currentCoord->data)->y);
			for(;currentCoord!=lastCoord;currentCoord=g_list_next(currentCoord))
			{

				pen_motion (((struct coord*)currentCoord->data)->x,((struct coord*)currentCoord->data)->y);
		
			}

				pen_up (((struct coord*)currentCoord->data)->x,((struct coord*)currentCoord->data)->y);
		

		  
		  }else
		{
				//printf("warning: current coord: %d, last coord: %d, history_len: %d\n", currentCoord, lastCoord, g_list_length(history));
		}
	  }

	  
	currentObject=((GList*)currentHistoryp->data);
	currentObject=g_list_next(currentObject);

	struct graphicObject* newVGO= ((struct graphicObject*)currentObject->data);
	loop_mode=newVGO->type;
	fingers=newVGO->fingers;
	variance=newVGO->variance;
	color=newVGO->color;
	alpha=newVGO->alpha;
	pen_radius=newVGO->pen_radius;


	   GList* currentCoord=g_list_next(currentObject);
	   GList* lastCoord=g_list_last(currentCoord);
	   if(currentCoord!=NULL&&lastCoord!=NULL)
	   {
		   pen_down(((struct coord*)currentCoord->data)->x,((struct coord*)currentCoord->data)->y);
			for(;currentCoord!=lastCoord;currentCoord=g_list_next(currentCoord))
			{
			
				pen_motion (((struct coord*)currentCoord->data)->x,((struct coord*)currentCoord->data)->y);
		
			}

				pen_up (((struct coord*)currentCoord->data)->x,((struct coord*)currentCoord->data)->y);
	
		}else
		{
				printf("warning: current coord: %d, last coord: %d, history_len: %d\n", currentCoord, lastCoord,
				g_list_length(history));
		}
	  
	  
	
}


double color2double(int color_value)
{
	return (color_value*1.0)/ 65535.0;
}
static void
paint (GtkWidget      *widget,
       GdkEventExpose *eev,
       gpointer        data)
{
  cairo_t *cr;
  cr = gdk_cairo_create (widget->window);
  drawapp_render (cr);
  cairo_destroy (cr);
  
}

static void
pen_motion (gdouble  x,
            gdouble  y)
{
  if (pen_is_down)
    {
      coords_add (x,y);
      gtk_widget_queue_draw (canvas);
    }
}

static void
pen_down (gdouble x,
          gdouble y)
{
  pen_is_down = TRUE;
  //add new object to history

  struct graphicObject* newVGO = (struct graphicObject*)malloc(sizeof(struct graphicObject));
  
  newVGO->color=color;
  newVGO->alpha=alpha;
  
  newVGO->pen_radius=pen_radius;
  
  
  if(loop_mode)
  {
	newVGO->type= LOOP;
    newVGO->fingers=1;
    fingers_old=fingers;
    fingers=1;
	newVGO->variance=0.0;
  }
  else
  {
	newVGO->type=  PEN;
  if(fingers_old> 1)
  {
	  fingers=fingers_old;
	  fingers_old=1;
	}
  newVGO->fingers=fingers;
  newVGO->variance=variance;
	}
  latestObj=g_list_alloc();
  latestRenderedObj=(GList*)malloc(sizeof(GList*)*fingers);
  for(int i=0; i< newVGO->fingers; i++)
	latestRenderedObj[i]=g_list_alloc();
 
  
  history=g_list_append(history, latestObj);
  for(int i=0; i< newVGO->fingers; i++)
	rendered=g_list_append(rendered, latestRenderedObj[i]);

  latestObj->data=newVGO;
  
  for(int i=0; i< newVGO->fingers; i++)
  {
	  struct graphicObject* newVGO_f = (struct graphicObject*)malloc(sizeof(struct graphicObject));
	    newVGO_f->color=color;
		newVGO_f->alpha=alpha;
  
		newVGO_f->pen_radius=pen_radius;
		
		newVGO_f->fingers=newVGO->fingers;
		newVGO_f->variance=variance;
		apply_variance(i, newVGO_f->fingers, newVGO_f->variance,
						&newVGO_f->pen_radius, &newVGO_f->color, &newVGO_f->alpha);
	  
	  if(loop_mode)
		newVGO_f->type= LOOP;
	  else
		newVGO_f->type=  PEN;
	  
	latestRenderedObj[i]->data=newVGO_f;
   }

  coords_add (x,y);
  gtk_widget_queue_draw (canvas);
}

static void
pen_up (gdouble x,
        gdouble y)
{
  coords_add (x,y);

  
  pen_is_down = FALSE;
  
  //add random end
  if(variance>0.0)
	finalise_stroke();
	
  apply_coords (buffer);
  
  coords_clear ();
  gtk_widget_queue_draw (canvas);
}


static void
init (void)
{
  coords_clear ();

  memset (buffer, 0, sizeof(buffer));
  back_surface = cairo_image_surface_create_for_data (buffer,
               CAIRO_FORMAT_ARGB32, WIDTH, HEIGHT, STRIDE);
               
 

  temp_surface = cairo_image_surface_create_for_data (temp_buffer,
               CAIRO_FORMAT_ARGB32, WIDTH, HEIGHT, STRIDE);
  memset (temp_buffer, 0, sizeof(temp_buffer));

  //seeed=time(NULL);
  
  srand(seeed);

  cr_save = cairo_create (temp_surface);
  int i=0;
  GdkColor b={0,0,0,0};
  for(i=0; i<20; i++)cHistory[i]=b;

  
}

int history_free(GList* historyp)
{
	GList* firstHistory= g_list_first(historyp);
	GList* pHistory= g_list_first(historyp);
	GList* lastHistory= g_list_last(historyp);
	for(;pHistory!=lastHistory;pHistory=pHistory->next)
	{
		free (((GList*)pHistory->data)->data);
		g_list_free((GList*)pHistory->data);
	}
	if ( g_list_length(lastHistory) )
	{
		free (((GList*)lastHistory->data)->data);
		g_list_free((GList*)lastHistory->data);
		return 0;
	}
	
	return 0;
}

static void
quit (void)
{
	//free history

	history_free(history);
	if(loaded_history)history_free(loaded_history);
	if(binary_data)free(binary_data);
	if(binary_data)free(binary_data);
	if(binary_data_target)free(binary_data_target);
	if(binary_data_to)free(binary_data_to);
	if(rendered)history_free(rendered);
	
	
}
/* just wrapping the gtk events */

static gboolean
event_press (GtkWidget      *widget,
             GdkEventButton *bev,
             gpointer        user_data)
{
  pen_down (bev->x, bev->y);
  return TRUE;
}

static gboolean
event_release (GtkWidget      *widget,
               GdkEventButton *bev,
               gpointer        user_data)
{
  pen_up (bev->x, bev->y);
  return TRUE;
}

static gboolean
event_motion (GtkWidget      *widget,
              GdkEventMotion *mev,
              gpointer        user_data)
{
  pen_motion (mev->x, mev->y);
  gdk_window_get_pointer (widget->window, NULL, NULL, NULL);

  return TRUE;
}

static void
change_color_callback ()
{
  GtkWidget *dialog;
  GtkColorSelection *colorsel;
  gint response;
  
  dialog = gtk_color_selection_dialog_new ("Changing color");

  //gtk_window_set_transient_for (GTK_WINDOW (dialog), GTK_WINDOW (window));

  colorsel = 
    GTK_COLOR_SELECTION (gtk_color_selection_dialog_get_color_selection (GTK_COLOR_SELECTION_DIALOG (dialog)));
  cHistory[cHisIdx]=color;
  gtk_color_selection_set_previous_color (colorsel, &color);
  gtk_color_selection_set_current_color (colorsel, &color);
  gtk_color_selection_set_has_palette (colorsel, TRUE);

  cHisIdx=(cHisIdx+1)%20;
  
  response = gtk_dialog_run (GTK_DIALOG (dialog));

  if (response == GTK_RESPONSE_OK)
    {
      gtk_color_selection_get_current_color (colorsel,
					     &color);
      
      gtk_widget_modify_bg (da, GTK_STATE_NORMAL, &color);
    }
  
  gtk_widget_destroy (dialog);
}




static gboolean
event_keypress (GtkWidget   *widget,
                GdkEventKey *kev,
                gpointer     user_data)
{
	char filename[24];
	int counter=0;
	int offset=0;
	uint8_t fileIsNew=FALSE;
	offset=strlen("sketch");
	
  switch (kev->keyval)
    {
	  case GDK_c:
	  
		change_color_callback();

		break;
	  case GDK_S:
      case GDK_s:
		
		sprintf(filename, "sketch");

		#if __gnu_linux__
		for(counter=0; counter < 1000 && !fileIsNew; )
		{
			
			sprintf(filename+offset, "%i", counter);
			#
			if( access( filename, F_OK ) != -1 ) {
			// file exists
				counter++;
			} else {
				fileIsNew=TRUE;
			}
		}
		#endif
		array_from_history(history, &binary_data, seeed);
	
		save_bin(filename, binary_data);
		save_svg(FALSE, &filename);
		offset=strlen(filename);
		sprintf(filename+offset, "_r.svg");
		save_svg(TRUE, filename);
		sprintf(filename+offset, "_p.png");
		save_png(filename);
		sprintf(filename+offset, "_b");
		printf("History has %d elements", g_list_length(history));
		save_bin(filename, binary_data);
		break;
	  case GDK_r:
		save_svg(1, "tst_r");
		break;
		
      case GDK_q:
		gtk_main_quit();
		break;
      case GDK_p:
		loop_mode = ! loop_mode;

		break;
      case GDK_f:
		pen_radius+=0.5;
		break;
      case GDK_d:
		pen_radius-=0.5;
		break;
      case GDK_x:
        if(alpha <= 0.9)alpha+=0.1;
        break;
      case GDK_y:
        if(alpha >= 0.1)alpha-=0.1;
        break;
	  case GDK_X:
	          if(variance <= 0.9)variance+=0.1;
	           break;
      case GDK_Y:
              if(variance >= 0.1)variance-=0.1;
               break;
	  case GDK_F:
		fingers++;
		break;
      case GDK_D:
		if(fingers>1)fingers--;
		break;
        
      default:
        break;
    }

  return TRUE;
}


gint
main (gint    argc,
      gchar **argv)
{

  GtkWidget *mainwin;

 if(argc > 1)doNanoSVG(argv[1]);
  
  gtk_init (&argc, &argv);

  mainwin = gtk_window_new (GTK_WINDOW_TOPLEVEL);

  canvas = gtk_drawing_area_new ();

  gtk_widget_set_events (canvas,
                         GDK_EXPOSURE_MASK            |
#ifdef USE_HINT
                         GDK_POINTER_MOTION_HINT_MASK |
#endif
                         GDK_BUTTON1_MOTION_MASK      |
                         GDK_BUTTON_PRESS_MASK        |
                         GDK_KEY_PRESS_MASK           |
                         GDK_BUTTON_RELEASE_MASK);
  gtk_widget_set_size_request (canvas, WIDTH, HEIGHT);

  g_signal_connect (mainwin, "destroy",
		    G_CALLBACK (gtk_main_quit), NULL);
  g_signal_connect (G_OBJECT (canvas), "expose-event",
                    G_CALLBACK (paint), NULL);
  g_signal_connect (G_OBJECT (canvas), "motion_notify_event",
                    G_CALLBACK (event_motion), NULL);
  g_signal_connect (G_OBJECT (canvas), "button_press_event",
                    G_CALLBACK (event_press), NULL);
  g_signal_connect (G_OBJECT (canvas), "button_release_event",
                    G_CALLBACK (event_release), NULL);
  g_signal_connect (G_OBJECT (canvas), "key_press_event",
                    G_CALLBACK (event_keypress), NULL);
  g_object_set (G_OBJECT (canvas), "can_focus", 1, NULL);

  gtk_container_add (GTK_CONTAINER (mainwin), canvas);

  init ();
  if(argc > 4)
  {
	  loadSVG_morph_batched(argv[1],argv[2], atof(argv[3]),argv[4] );

  quit();
  return 0;
  }
  else if(argc > 3)loadSVG_morph(argv[1],argv[2], atof(argv[3]));
  else if(argc > 2)loadOld(argv[1], argv[2]);
  else if(argc > 1)loadSVG(argv[1]);
  gtk_widget_show_all (mainwin);

 
  
  gtk_main ();
  quit();
  return 0;
}





/* utility function that creates a smooth path from a set
 * of coordinates
 */
static void
points_to_bezier_path (cairo_t *cr,
                       gdouble  coord_x[],
                       gdouble  coord_y[],
                       gint     n_coords)
{
	//printf("n_coords%d\n",n_coords);
	
  gint    i;
  int toRem=0;
  gdouble smooth_value;
 
  smooth_value  = 1;

  cairo_new_path (cr);

  if (!n_coords)
    return;
    
    if(!loop_mode && coord_count%4!=0)
    {
	
		int i=0;
		toRem=coord_count%4;
		for(;i<toRem;i++)
		{
			coord_x[coord_count+i]=coord_x[coord_count];
			coord_y[coord_count+i]=coord_y[coord_count];
			
		}
		coord_count+=toRem;
	}

  cairo_move_to (cr, coord_x[0], coord_y[0]);

  for (i=1;i<n_coords;i++)
    {
      gdouble x2 = coord_x[i];
      gdouble y2 = coord_y[i];

      gdouble x0,y0,x1,y1,x3,y3;

      if (i==1)
        {
          x0=coord_x[i-1];
          y0=coord_y[i-1];
          x1 = coord_x[i-1];
          y1 = coord_y[i-1];
        }
      else
        {
          x0=coord_x[i-2];
          y0=coord_y[i-2];
          x1 = coord_x[i-1];
          y1 = coord_y[i-1];
        }

      if (i<n_coords+1)
        {
          x3 = coord_x[i+1];
          y3 = coord_y[i+1];
        }
      else
        {
          x3 = coord_x[i];
          y3 = coord_y[i];
        }

      {
        gdouble xc1 = (x0 + x1) / 2.0;
        gdouble yc1 = (y0 + y1) / 2.0;
        gdouble xc2 = (x1 + x2) / 2.0;
        gdouble yc2 = (y1 + y2) / 2.0;
        gdouble xc3 = (x2 + x3) / 2.0;
        gdouble yc3 = (y2 + y3) / 2.0;

        gdouble len1 = sqrt( (x1-x0) * (x1-x0) + (y1-y0) * (y1-y0) );
        gdouble len2 = sqrt( (x2-x1) * (x2-x1) + (y2-y1) * (y2-y1) );
        gdouble len3 = sqrt( (x3-x2) * (x3-x2) + (y3-y2) * (y3-y2) );

        gdouble k1 = len1 / (len1 + len2);
        gdouble k2 = len2 / (len2 + len3);

        gdouble xm1 = xc1 + (xc2 - xc1) * k1;
        gdouble ym1 = yc1 + (yc2 - yc1) * k1;

        gdouble xm2 = xc2 + (xc3 - xc2) * k2;
        gdouble ym2 = yc2 + (yc3 - yc2) * k2;

        gdouble ctrl1_x = xm1 + (xc2 - xm1) * smooth_value + x1 - xm1;
        gdouble ctrl1_y = ym1 + (yc2 - ym1) * smooth_value + y1 - ym1;

        gdouble ctrl2_x = xm2 + (xc2 - xm2) * smooth_value + x2 - xm2;
        gdouble ctrl2_y = ym2 + (yc2 - ym2) * smooth_value + y2 - ym2;
        
        cairo_curve_to (cr, ctrl1_x, ctrl1_y, ctrl2_x, ctrl2_y, x2,y2);
      }
   }

  if(loop_mode)
	{
  	cairo_close_path (cr);
	cairo_fill (cr);
	}
	coord_count-=toRem;
}



static void
points_to_linear_path_fingers_list (cairo_t *cr,
                       gint     n_coords)
{
  gint i;

  if (n_coords<3)
    return;
    
    for(int i=0; i< fingers; i++)
    {
		GList* first=g_list_first(latestRenderedObj[i]);
		GList* last=g_list_last(latestRenderedObj[i]);

		GList* pCoord=first;
		struct graphicObject* go=(struct graphicObject*)first->data;
		cairo_set_line_width (cr, go->pen_radius*2);
		cairo_set_source_rgba (cr, (go->color.red*1.0)/ 65535.0,(go->color.green*1.0)/ 65535.0,(go->color.blue*1.0)/ 65535.0, go->alpha);
		cairo_set_line_join (cr, CAIRO_LINE_JOIN_ROUND);
		cairo_set_line_cap (cr, CAIRO_LINE_CAP_ROUND);
		 // try variable startpoints
		if(n_coords==3 && variance)
		{	GList* first=pCoord->next;
			gdouble x0=((struct coord*)first->data)->x;
			gdouble y0=((struct coord*)first->data)->y;
			GList* second=first->next;
			gdouble x1=((struct coord*)second->data)->x;
			gdouble y1=((struct coord*)second->data)->y;
			gdouble fact=((rand()%10)*1.0)/10.0;
			gdouble dx=(x1-x0)*fact;
			gdouble dy=(y1-y0)*fact;
			((struct coord*)first->data)->x=x0+dx;
			((struct coord*)first->data)->y=y0+dy;
			//printf("%f, %f, %f X, %f, Y, %f %f %f\n", fact, dx, dy,x0, y0, x1, y1 );
			
		}
		pCoord=pCoord->next;
		if(pCoord && pCoord->data)
		{
			double x=0.0f;
			double y=0.0f;
			x=((struct coord*)pCoord->data)->x;
			y=((struct coord*)pCoord->data)->y;
			//printf("%d: %f, %f\n",i, x, y);
			cairo_move_to (cr, x, y);
			
			for(pCoord=pCoord->next;(pCoord!=NULL)&&pCoord!=last;pCoord=pCoord->next)
			{
				if (pCoord!=NULL){
					x=((struct coord*)pCoord->data)->x;
					y=((struct coord*)pCoord->data)->y;
					
					cairo_line_to (cr, x, y);
				}
				//printf("%d: %f, %f\n",i, x, y);
			}
			
			if(pCoord==last)
				{
				x=((struct coord*)pCoord->data)->x;
				y=((struct coord*)pCoord->data)->y;

				cairo_line_to (cr, x, y);
				}
			
		  if(loop_mode)
			{
			cairo_close_path (cr);
			cairo_fill (cr);
			}

		}
		if(!loop_mode)
	{
		cairo_stroke (cr);
	
	}	
	}

}



/* this function is included, just to be
 * able to check the improvement over the easiest method
 */
static void
points_to_linear_path (cairo_t *cr,
                       gdouble  coord_x[],
                       gdouble  coord_y[],
                       gint     n_coords)
{
  gint i;

  if (!n_coords)
    return;
  cairo_move_to (cr, coord_x[0], coord_y[0]);

  for (i = 1; i < n_coords; i++)
    cairo_line_to (cr, coord_x[i], coord_y[i]);
    
  if(loop_mode)
	{
  	cairo_close_path (cr);
	cairo_fill (cr);
	}
}
