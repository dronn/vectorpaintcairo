
#ifndef BINARY_FORMAR_H
#define BINARY_FORMAR_H

#include <gtk/gtk.h>
#include <cairo.h>
#include <math.h>
#include <gdk/gdkkeysyms.h>
#include <string.h>
#include <glib.h>
#include <stdint.h>
#include <malloc.h>
#include <locale.h> 
#include <stdlib.h>		
#include "graphics_object.h"

#define HEADER_SIZE  3
#define STROKE_HEADER_SIZE 9
// header: 0 seed 1 number of strokes 2 overall size


#ifndef  READALL_CHUNK
#define  READALL_CHUNK  262144
#endif

#define  READALL_OK          0  /* Success */
#define  READALL_INVALID    -1  /* Invalid parameters */
#define  READALL_ERROR      -2  /* Stream error */
#define  READALL_TOOMUCH    -3  /* Too much input */
#define  READALL_NOMEM      -4  /* Out of memory */


// strokes' header: 0 type, 1 radius, 2 fingers 3 variance, 4 red, 5 green, 6 blue, 7 alpha, 8 num_coords

//#define STROKE_TYPE 0
//#define RADIUS 1
//#define FINGERS 2
//#define VARIANCE 3
//#define ALPHA 7
//#define NUMCOORDS 8

struct strokesHeader
{
	uint16_t type;
	uint16_t radius;
	uint16_t fingers;
	uint16_t variance;
	uint16_t red;
	uint16_t green;
	uint16_t blue;
	uint16_t alpha;
	uint16_t coord_size;
};

void array_from_history(GList* history, uint16_t** data, uint seeed);
		
void save_bin(const char* filename, uint16_t* data);

void load_bin(const char* filename, uint16_t** data);

void populate_history(uint16_t* data, uint16_t* seeed, GList** history);

void print_sketch_debug(uint16_t** data, uint16_t level);

#endif
