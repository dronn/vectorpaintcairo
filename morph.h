#ifndef MORPH_H
#define MORPH_H

#include <gtk/gtk.h>
#include <cairo.h>
#include <math.h>
#include <gdk/gdkkeysyms.h>
#include <string.h>
#include <glib.h>
#include <stdint.h>
#include <malloc.h>
#include <locale.h> 
#include <stdlib.h>		

uint16_t linear_interpolation(uint16_t shorter_value,  uint16_t longer_value, double stage_to_len);

uint16_t shrink_interpolation(uint16_t shorter_value, uint16_t  longer_value, uint16_t current_delta_to_shorter, uint16_t delta_to_shorter, double stage_to_len);

void linear_interpolation_pair(uint16_t shorter_value_x, uint16_t shorter_value_y,

	  uint16_t longer_value_x, uint16_t longer_value_y,
	  
	  double stage_to_len, uint16_t* return_x, uint16_t* return_y);

void shrink_interpolation_pair(uint16_t shorter_value_x, uint16_t shorter_value_y,

	 uint16_t  longer_value_x, uint16_t  longer_value_y,
	 
	 uint16_t current_delta_to_shorter, uint16_t delta_to_shorter, double stage_to_len, uint16_t* return_x, uint16_t* return_y);


uint16_t morph_path(uint16_t* from, uint16_t from_len, uint16_t* to, uint16_t to_len, uint16_t** target, uint16_t target_offset, uint16_t target_len, double stage);	
	
void morph(uint16_t* from, uint16_t* to, uint16_t** target, double_t stage );

#endif
