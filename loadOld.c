#include "loadOld.h"
#include "binary_format.h"
#include <locale.h>  
uint16_t load_old(const char* sketch, const char* rendered_sketch, uint16_t** data)
{
	uint8_t issvg=0;
	uint8_t file_error=0;
	char* sketch_data, *rendered_sketch_data;
	uint16_t* local_data=0;
	uint16_t seeed=1227;
	uint16_t path_count=0;
	uint16_t coord_size=0;
	uint16_t data_size=0;
	uint16_t stroke_header_size=STROKE_HEADER_SIZE;
	uint16_t header_size=HEADER_SIZE;
	uint16_t coord_offset=0;
	uint16_t current_coord_offset=0;
	
	printf("interpreting old files");
	
	file_error=loadFile2string(sketch, &sketch_data);
	if(file_error) return;
	
	file_error=loadFile2string(rendered_sketch, &rendered_sketch_data);

	if(file_error) return;
	
	issvg=is_svg(sketch_data);
	
	if(!issvg)return 1;

	issvg=is_svg(rendered_sketch_data);

	if(!issvg)return 1;
	
	printf("found path  %d in %s \n", get_stroke_num(sketch_data), sketch);
	path_count=get_stroke_num(sketch_data);
	printf("found path  %d in %s \n", get_stroke_num(rendered_sketch_data), rendered_sketch);
	//get_stroke_num(rendered_sketch_data);
	
	int i=0;
	char* pos=sketch_data;
	//get coord size
	for(i=0; i< path_count; i++)
	{
			int path_size=0;
			pos=strstr (pos, "       d=\"M");
		//printf("found path at %d \n", pos);
		if(!pos)break;
		else {
			path_size=get_path_size(pos);
			coord_size=coord_size+path_size;
			pos=pos+8;
			
			
			}
	}
	
	
	//allocate data
	
	data_size= header_size+ path_count*stroke_header_size+ coord_size*2+2;
	
	 local_data=(uint16_t*) malloc( (data_size -1) * sizeof(uint16_t) );
	 
	 memset(local_data, 0, data_size);
	 local_data[0]=seeed;
	 local_data[1]=path_count;
	 local_data[2]=data_size;
	 printf("seed %d\n", seeed);
	 printf("path count %d\n", path_count);
	 printf("size count %d\n", data_size);
	


	coord_offset=header_size+ (path_count)*stroke_header_size;
	current_coord_offset=coord_offset;	
	
	//get path properties
	pos=sketch_data;
	char* properies_pos=sketch_data;
	print_sketch_debug(&local_data, 1);
	
	
	for(i=0; i< path_count; i++)
	{
			uint16_t path_size=0;
			pos=strstr (pos, "       d=\"M");
			path_size=get_path_size(pos);
			printf("offset %d, current offset %d\n",coord_offset, current_coord_offset);
			local_data[HEADER_SIZE+STROKE_HEADER_SIZE*i+8]=path_size;
			
			path_from_string( pos, &local_data, current_coord_offset, path_size);
			properies_pos=strstr (pos, "style=\"opacity:");
			if(properies_pos)
				get_stroke_properties(properies_pos, &local_data, HEADER_SIZE+i*STROKE_HEADER_SIZE);
			else break;
			
			printf("path size: %d\n", path_size);
			current_coord_offset=current_coord_offset+path_size*2;
			
		if(!pos)break;
		else {
			
			pos=pos+8;
			
			
			}
	}
	
	*data=local_data;
	
//for(i=0; i< data_size; i++)
//{
//printf(" %d ", local_data[i]);	
//}
//printf("\n");
//

	//printf("%s \n\n", rendered_sketch_data);
	//print_sketch_debug(&local_data);
	
	if(rendered_sketch_data)free(rendered_sketch_data);
	if(sketch_data)free(sketch_data);
	
return 0;
	
}

double guess_variance(double stroke, double* rendered_strokes)
{
	double result=0.0;

	return 	result;
}

uint16_t  guess_fingers(char* path, char* rendered_pathes)
{
	return 0;
}

void colour_from_string(char* string, uint16_t** colour)
{

	char hexstring[7];
		int i=0;
	for(i=0; i<7; i++)hexstring[i]=string[i+1];
	hexstring[6]='\0';
	//printf("### %s\n", hexstring);
	if(string[0]=='\#')
	{
	
		for(i=0; i<3; i++)
		{
			(*colour)[i]=0;
			//printf("??%d %c, %c \n",i,hexstring[i*2], hexstring[i*2+1]  );
			
			if(hexstring[i*2] >= 48 && hexstring[i*2] < 58)
			{
				(*colour)[i]=(hexstring[i*2]-48);
				(*colour)[i]=(*colour)[i]*16;
				//printf("<< %c: %d \n",hexstring[i*2], (*colour)[i] );
			}
			if(hexstring[i*2] >= 97 && hexstring[i*2] < 103)
			{
				(*colour)[i]=(hexstring[i*2]-87);
				(*colour)[i]=(*colour)[i]*16;
				//printf(">> %c: %d \n",hexstring[i*2], (*colour)[i] );
			}
			//second hex
			if(hexstring[i*2+1] >= 48 && hexstring[i*2+1] < 58)
			{
				(*colour)[i]=(*colour)[i]+(hexstring[1+i*2]-48);
				//printf("++ %c: %d \n",hexstring[i*2+1], (*colour)[i] );
			}
			if(hexstring[i*2+1] >= 97 && hexstring[i*2+1] < 103)
			{
				(*colour)[i]=(*colour)[i]+(hexstring[i*2+1]-87);
				//printf("-- %c: %d \n",hexstring[i*2+1], (*colour)[i] );
			}
			(*colour)[i]=(*colour)[i]*255;
			//printf(" calculated colour %d \n",(*colour)[i] );
		}
	}else
	{
			printf("no colour string");
	}
	
}
uint8_t is_svg( char* string)
{
	//<?xml version="1.0" encoding="UTF-8" standalone="no"?>
	//<svg
	
	if(strcmp(string, "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>")> 0)
	{
		printf("is svg\n");
		return 1;

	}
	printf("is not svg\n");
	return 0;	
}



void get_stroke_properties(char* svg_string, uint16_t** header, uint16_t offset)
{
	if(!svg_string)return;
	
	char *old_locale=setlocale(LC_ALL,NULL);
	setlocale(LC_ALL,"");
	setlocale(LC_ALL,"C");
	if(strcmp(svg_string, "style=\"opacity:"))
	{
		char* pos=svg_string;
		char number[10];
		double alpha=0.3;
		double variance=0.0;
		double pen_size=0.5;
		uint16_t* colour;
		uint16_t fingers=1;
		// strokes' header: 0 type, 1 radius, 2 fingers 3 variance, 4 red, 5 green, 6 blue, 7 alpha, 8 num_coords
		uint16_t type=1;
		//opacity: 
		//stroke-width:
		//stroke:#800080
		//fill:#000000
		//if fill: none -> type pen
		colour=(uint16_t*)malloc(3*sizeof(uint16_t));
		
		pos=strstr (svg_string, "opacity:");
		pos=strstr (svg_string, "fill:");
		if(pos[strlen("fill:")]=='n')
		{
			type=0;
		}
		
		pos=strstr (svg_string, "opacity:");
		int i=0;
		memset(number, 0, 10);
		for(i=0; i<10; i++)
		{
			number[i]=pos[strlen("opacity:")+i];
		}
		
		number[9]='\0';
		
	
		alpha=atof(number);
		//printf("alpha %s, %f \n", number, alpha);
		if(!type)
		{
			pos=strstr (svg_string, "fill:");
			pos=strstr (pos, "#");
			colour_from_string(pos, &colour);
			
						pos=strstr (svg_string, "stroke-width:");
			memset(number, 0, 10);
			
			for(i=0; i<10; i++)
			{
				number[i]=pos[strlen("stroke-width:")+i];
				
			}
			number[9]='\0';
			
			pen_size=atof(number);
		
			
		}else
		{
			pos=strstr (svg_string, "stroke:");
			pos=strstr (pos, "#");
			colour_from_string(pos, &colour);
			

		}
		
			(*header)[0+offset]=(uint16_t)(type);
			(*header)[1+offset]=(uint16_t)(pen_size*50.0);
			(*header)[2+offset]=(uint16_t)fingers;
			(*header)[3+offset]=(uint16_t)(variance*100.0);
			(*header)[4+offset]=(uint16_t)colour[0];
			(*header)[5+offset]=(uint16_t)colour[1];
			(*header)[6+offset]=(uint16_t)colour[2];
			(*header)[7+offset]=(uint16_t)(alpha*100.0);
		
		free( colour);
	}
	else
	{
		printf("aint no style\n");
	}
	setlocale (LC_ALL,old_locale);
	
}


//path size in coordinate tuples
uint16_t get_path_size( char * pathString)
{
	char * line=0;
	int line_len=0;
	int i=0;
	
	if(!pathString)return 0;
	
	for(line_len=0; line_len< strlen(pathString)-1; line_len++)
	{
			if(pathString[line_len]=='\n')break;
	}
	
	line= (char*)malloc(line_len);
	if(!line)return 0;
	
	for(i=0; i< line_len; i++)
	{
		line[i]=pathString[i];
	}
	line[line_len-1]='\0';
	//printf("%s\n", line);
	
	if(strcmp(pathString, "       d=\"M")> 0)
	{
		if(strstr (pathString, "       d=\"M")==pathString)
		{
		
			int count=0;
			
			count=count_substring(line, ",");
			
			if(line)
				free(line);
			return count;
		}
	}
	if(line)free(line);
	return 0;
}

void path_from_string( char* string, uint16_t** path, uint16_t offset, uint16_t path_len)
{
	//"       d="M"

	char * line=0;
	int line_len=0;
	int i=0;
	char* pos=0;
	char number[7];//single coord string
	uint16_t coordinate=0;
	int j=0;
	pos=string;
	//printf("line #00: %s %d \n",pos, pos);
	pos=strstr (pos, "M");
	
	if(!pos)return;
	
	for(line_len=0; line_len< strlen(pos); line_len++)
	{
			if(pos[line_len]=='\n')
			{
				line_len++;

				break;
			}
			if(pos[line_len]=='\"')
			{

				line_len++;
				break;
			}
	}
	
	line= (char*)malloc(line_len+2);
	if(!line)
	{
		memset((*path), 0, path_len);
		return;
	}
	for(i=0; i< line_len; i++)
	{
		line[i]=pos[i];
	}
	line[line_len]='\0';
	pos=line;
	//printf("line #0: %s %d \n",pos, pos);
	pos=strstr (pos, "M");
	if(!pos)
	{
		if(line)free(line);
		return;
	
	}

	pos++;
	pos++;
	//printf("line #1: %s %d \n",pos, pos);
	memset(number, 0, 7);
	
	for(j=0; j<7; j++)
		{
			if(pos[j]=='\0')break;
		if(pos[j]!=' ' && pos[j]!=',')
		{
			number[j]=pos[j];
			
			
		}else break;

			
	}
		number[6]='\0';
		
		coordinate=atoi(number);
		(*path)[0+offset]=coordinate;



		
	pos=strstr (pos, ", ");
	pos++;
	pos++;
	memset(number, 0, 7);
	for(j=0; j<7; j++)
	{
		if(pos[j]=='\0')break;
		
		if(pos[j]!=' ' &&pos[j]!=',')
		{
			number[j]=pos[j];
			
			
		}else break;

			
	}
	number[6]='\0';
	
	coordinate=atoi(number);
	(*path)[1+offset]=coordinate;




	
	
	pos=strstr (pos, "L");
		if(!pos)return;

	pos++;
	pos++;
	memset(number, 0, 7);
	for(j=0; j<7; j++)
	{
		
	if(pos[j]=='\0')break;
	
	if(pos[j]!=' ' &&pos[j]!=',')
		{
			number[j]=pos[j];
		
				
		}else break;

			
	}
	number[6]='\0';
	
	coordinate=atoi(number);
	(*path)[2+offset]=coordinate;



		
	
	
	for(i=3; i< path_len*2; i++)
	{
		if(i%2==1)pos=strstr (pos, ",");
		else pos=strstr (pos, " ");
		
		//printf("found path at %d \n", pos);
		if(!pos)break;
		else
		{
			
			pos++;

			memset(number, 0, 7);
			for(j=0; j<7; j++)
			{
				if(pos[j]=='\0')break;
					if(pos[j]!=' ' &&pos[j]!=','&& pos[j]!='\"')
					{
						number[j]=pos[j];
						
						
					}else break;

					
			}
					number[6]='\0';
					
					coordinate=atoi(number);
					if(coordinate)
					{
						if((i+offset)<(*path)[2])
							(*path)[i+offset]=coordinate;
						//printf("coord: %s (s); %d \n", number, coordinate);
					}

			
			
		}
	}
	
	if(line)free(line);
	
}

uint16_t loadFile2string(const char* filename, char** string)
{
	
	
FILE *ptr_myfile=0;
	uint16_t file_size=0;
	char  *_data = NULL, *temp;
	size_t size = 0;
    size_t used = 0;
    char * local_data=0;
    size_t n;
	

		ptr_myfile=fopen(filename,"r");

		if (!ptr_myfile)
		{
			printf("Unable to open file!");
			return ;
		}
		if (ferror(ptr_myfile))
        return;



	    while (1) {

        if (used + READALL_CHUNK + 1 > size) {
            size = used + READALL_CHUNK + 1;

            /* Overflow check. Some ANSI C compilers
               may optimize this away, though. */
            if (size <= used) {
                free(_data);
                return READALL_TOOMUCH;
            }

            temp = realloc(_data, size);
            if (temp == NULL) {
                free(_data);
                return READALL_NOMEM;
            }
            _data = temp;
        }

        n = fread(_data + used, 1, READALL_CHUNK, ptr_myfile);
        if (n == 0)
            break;

        used += n;
    }

    if (ferror(ptr_myfile)) {
        free(_data);
        return READALL_ERROR;
    }

  printf("Loaded %i\n", used);

    local_data=  malloc( used );
    int i=0;
    for(i=0; i < used; i++)
		local_data[i]=_data[i];
		printf("local data %d\n", local_data);
	*string=(char*)local_data;
	
	return 0;
}

uint16_t count_substring(char* string, const char* substring)
{
	int count=0;
	char* pos=string;
	
	for(count=0; count< strlen(string)-1;count++)
	{
		pos=strstr (pos, substring);
		//printf("found path at %d \n", pos);
		if(!pos)break;
		else pos++;
		
	}
	return count;	
}


uint16_t get_stroke_num(char* svg_string)
{
	//<path
	int count=0;
    count=count_substring(svg_string, "<path");

	return count;	
}
