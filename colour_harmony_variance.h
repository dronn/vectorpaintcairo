#ifndef COLOUR_HARMONY_VARIANCE_H
#define COLOUR_HARMONY_VARIANCE_H

#include <gtk/gtk.h>
#include <cairo.h>
#include <math.h>
#include <gdk/gdkkeysyms.h>
#include <string.h>
#include <glib.h>
#include <stdint.h>
#include <malloc.h>
#include <locale.h> 
#include <stdlib.h>		
		
void apply_variance(int index, int fingers, gdouble variance,
						gdouble* pen_radius, GdkColor *color, gdouble *alpha);

#endif
