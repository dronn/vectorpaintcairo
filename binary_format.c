#include "binary_format.h"
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>




//struct graphicObject
//{
//
//	enum vectorObjectIdentifier type;
//
//	double pen_radius;
//	gint fingers;
//	gdouble variance;
//
//	GdkColor color;
//	double alpha;
//};


void array_from_history(GList* history, uint16_t** data, uint seeed)
{
	uint16_t number_of_strokes= g_list_length (history);
	uint16_t overall_coordinates=0;

		GList* firstHistory= NULL;
		GList* pHistory= NULL;
		GList* lastHistory= NULL;

	 firstHistory= g_list_first(history);
	 pHistory= g_list_first(history);
	 lastHistory= g_list_last(history);
		

	
	GList* firstCoord;
	GList* pCoord;
	GList* lastCoord;
	
   // header: 0 seed 1 number of strokes 2 overall size
	uint16_t header_size= HEADER_SIZE;
	// strokes' header: 0 type, 1 radius, 2 fingers 3 variance, 4 red, 5 green, 6 blue, 7 alpha, 8 num_coords
	uint16_t stroke_header_size=STROKE_HEADER_SIZE;
	//determine size: header
	//number of strokes* strokes' header
	//coordinates of all strokes
	uint16_t data_size=0;
	uint16_t* local_data=NULL;
	
	uint16_t coord_offset=0;
	uint16_t coord_counter=0;

	int i=0;
	for(;pHistory!=lastHistory;pHistory=pHistory->next, i++)
	{
		firstCoord=(GList*)pHistory->data;
		uint16_t coord_len=g_list_length(firstCoord) -1;
		
		overall_coordinates+=coord_len;
		

			
	}
		firstCoord=(GList*)pHistory->data;
		uint16_t coord_len=g_list_length(firstCoord) -1;
		
		overall_coordinates+=coord_len;
	
	data_size= header_size+ number_of_strokes*stroke_header_size+ overall_coordinates*2;
	coord_offset=header_size+ number_of_strokes*stroke_header_size;
	
	printf("we have %i strokes with %i overall coordinates, data_size= %i \n", number_of_strokes, overall_coordinates, data_size);
	
	 local_data=(uint16_t*) malloc( (data_size -1) * sizeof(uint16_t) );
	 
	local_data[0]=seeed;
	local_data[1]=number_of_strokes;
	local_data[2]=data_size;
	
	 i=0;
	 pHistory=g_list_first(pHistory);
	for(;pHistory!=lastHistory;pHistory=pHistory->next, i++)

	{
		firstCoord=(GList*)pHistory->data;

		
		if(firstCoord){
		pCoord=firstCoord->next;
		
		lastCoord=g_list_last(firstCoord);
		}
			if(pCoord){
			
			struct graphicObject* header =(struct graphicObject*)firstCoord->data;
	// strokes' header: 0 type, 1 radius, 2 fingers 3 variance, 4 red, 5 green, 6 blue, 7 alpha, 8 num_coords

			local_data[header_size+i*stroke_header_size]=(uint16_t)(header->type);
			local_data[header_size+i*stroke_header_size+1]=(uint16_t)(header->pen_radius*100.0);
			local_data[header_size+i*stroke_header_size+2]=(uint16_t)header->fingers;
			local_data[header_size+i*stroke_header_size+3]=(uint16_t)(header->variance*100.0);
			local_data[header_size+i*stroke_header_size+4]=(uint16_t)header->color.red;
			local_data[header_size+i*stroke_header_size+5]=(uint16_t)header->color.green;
			local_data[header_size+i*stroke_header_size+6]=(uint16_t)header->color.blue;
			local_data[header_size+i*stroke_header_size+7]=(uint16_t)(header->alpha*100.0);
			local_data[header_size+i*stroke_header_size+8]=(uint16_t)g_list_length(firstCoord) -1;
			
			
			for(;pCoord!=lastCoord;pCoord=pCoord->next)
			{
				local_data[coord_offset+coord_counter]=((struct coord*)pCoord->data)->x;
				local_data[coord_offset+coord_counter+1]=((struct coord*)pCoord->data)->y;
				
				coord_counter+=2;	
			}
				local_data[coord_offset+coord_counter]=((struct coord*)pCoord->data)->x;
				local_data[coord_offset+coord_counter+1]=((struct coord*)pCoord->data)->y;
				
				coord_counter+=2;	
			
	}
	}
	
			firstCoord=(GList*)pHistory->data;

		
		
		pCoord=firstCoord->next;
		
		lastCoord=g_list_last(firstCoord);
			if(pCoord){
			
			struct graphicObject* header =(struct graphicObject*)firstCoord->data;
	// strokes' header: 0 type, 1 radius, 2 fingers 3 variance, 4 red, 5 green, 6 blue, 7 alpha, 8 num_coords

			local_data[header_size+i*stroke_header_size]=(uint16_t)(header->type);
			local_data[header_size+i*stroke_header_size+1]=(uint16_t)(header->pen_radius*100.0);
			local_data[header_size+i*stroke_header_size+2]=(uint16_t)(header->fingers);
			local_data[header_size+i*stroke_header_size+3]=(uint16_t)(header->variance*100.0);
			local_data[header_size+i*stroke_header_size+4]=(uint16_t)header->color.red;
			local_data[header_size+i*stroke_header_size+5]=(uint16_t)header->color.green;
			local_data[header_size+i*stroke_header_size+6]=(uint16_t)header->color.blue;
			local_data[header_size+i*stroke_header_size+7]=(uint16_t)(header->alpha*100.0);
			local_data[header_size+i*stroke_header_size+8]=(uint16_t)g_list_length(firstCoord) -1;
			
			
			for(;pCoord!=lastCoord;pCoord=pCoord->next)
			{
				local_data[coord_offset+coord_counter]=((struct coord*)pCoord->data)->x;
				local_data[coord_offset+coord_counter+1]=((struct coord*)pCoord->data)->y;
				
				coord_counter+=2;	
			}
				local_data[coord_offset+coord_counter]=((struct coord*)pCoord->data)->x;
				local_data[coord_offset+coord_counter+1]=((struct coord*)pCoord->data)->y;
				
				coord_counter+=2;	
			
	}
	
	
	
	
	
	*data=local_data;
	
	firstCoord=(GList*)pHistory->data;
	pCoord=firstCoord->next;
	lastCoord=g_list_last(firstCoord);



}

void print_sketch_debug(uint16_t** data, uint16_t level)
{
printf("header:\n")	;
printf("seed: %d\n", (*data)[0]);
printf("strokes: %d\n", (*data)[1]);
printf("coords: %d\n", (*data)[2]);
printf("strokes header:\n");
int i=0;
uint16_t coord_count=0;
	for(i=0; i<(*data)[1]; i++)
	{// strokes' header: 0 type, 1 radius, 2 fingers 3 variance, 4 red, 5 green, 6 blue, 7 alpha, 8 num_coords
		if(level > 0){
		printf("\n stroke %d \n", i);
		printf("type: %d\n",      (*data)[HEADER_SIZE+ i*STROKE_HEADER_SIZE]);
		printf("radius: %d\n",    (*data)[HEADER_SIZE+ i*STROKE_HEADER_SIZE+1]);
		printf("fingers: %d\n",   (*data)[HEADER_SIZE+ i*STROKE_HEADER_SIZE+2]);
		printf("variance: %d\n",  (*data)[HEADER_SIZE+ i*STROKE_HEADER_SIZE+3]);
		printf("red: %d\n",       (*data)[HEADER_SIZE+ i*STROKE_HEADER_SIZE+4]);
		printf("green: %d\n",     (*data)[HEADER_SIZE+ i*STROKE_HEADER_SIZE+5]);
		printf("blue: %d\n",      (*data)[HEADER_SIZE+ i*STROKE_HEADER_SIZE+6]);
		printf("alpha: %d\n",     (*data)[HEADER_SIZE+ i*STROKE_HEADER_SIZE+7]);
		printf("num coords: %d\n",(*data)[HEADER_SIZE+ i*STROKE_HEADER_SIZE+8]);
		}
		coord_count+=(*data)[HEADER_SIZE+ i*STROKE_HEADER_SIZE+8];	
	}
printf("coord_count: %d; first coordinate: %d\n",coord_count, (*data)[HEADER_SIZE+ (STROKE_HEADER_SIZE*(*data)[1])]);

}

		
void save_bin(const char* filename, uint16_t* data)
{
	FILE *ptr_myfile;
	
	ptr_myfile=fopen(filename,"wb");
	if (!ptr_myfile)
		{
			printf("Unable to open file!");
			return ;
		}
	fwrite(data, sizeof(uint16_t), data[2] , ptr_myfile);
	fclose(ptr_myfile);
}

void load_bin(const char* filename, uint16_t** data)
{
	FILE *ptr_myfile=0;
	uint16_t file_size=0;
	char  *_data = NULL, *temp;
	size_t size = 0;
    size_t used = 0;
    char * local_data=0;
    size_t n;
	

		ptr_myfile=fopen(filename,"rb");

		if (!ptr_myfile)
		{
			printf("Unable to open file!");
			return ;
		}
		if (ferror(ptr_myfile))
        return;



	    while (1) {

        if (used + READALL_CHUNK + 1 > size) {
            size = used + READALL_CHUNK + 1;

            /* Overflow check. Some ANSI C compilers
               may optimize this away, though. */
            if (size <= used) {
                free(_data);
                return READALL_TOOMUCH;
            }

            temp = realloc(_data, size);
            if (temp == NULL) {
                free(_data);
                return READALL_NOMEM;
            }
            _data = temp;
        }

        n = fread(_data + used, 1, READALL_CHUNK, ptr_myfile);
        if (n == 0)
            break;

        used += n;
    }

    if (ferror(ptr_myfile)) {
        free(_data);
        return READALL_ERROR;
    }

  printf("Loaded %i\n", used);

    local_data=  malloc( used );
    int i=0;
    for(i=0; i < used; i++)
		local_data[i]=_data[i];
		printf("local data %d\n", local_data);
	*data=(uint16_t*)local_data;
    //*sizeptr = used;
}

void populate_history(uint16_t* data, uint16_t* seeed, GList** history)
{
	*seeed=data[0];
	uint16_t num_strokes=data[1];
	uint16_t processed_coords=0;
	GList* latestObj;
	int i=0;
	
	//mind the header
	if(data[2]<HEADER_SIZE+num_strokes*STROKE_HEADER_SIZE)return;
	
	
	for(i=0; i< num_strokes ; i++)
	{
			
		  struct graphicObject* newVGO = (struct graphicObject*)malloc(sizeof(struct graphicObject));
			// strokes' header: 0 type, 1 radius, 2 fingers 3 variance, 4 red, 5 green, 6 blue, 7 alpha, 8 num_coords



			newVGO->type= data[HEADER_SIZE+ i* STROKE_HEADER_SIZE] ;
			newVGO->pen_radius= (double)data[HEADER_SIZE+ i* STROKE_HEADER_SIZE+1]/100.0;
			newVGO->fingers=    data[HEADER_SIZE+ i* STROKE_HEADER_SIZE+2];
			newVGO->variance=   (double)data[HEADER_SIZE+ i* STROKE_HEADER_SIZE+3] /100.0;
			newVGO->color.red=  data[HEADER_SIZE+ i* STROKE_HEADER_SIZE+4];
			newVGO->color.green=data[HEADER_SIZE+ i* STROKE_HEADER_SIZE+5];
			newVGO->color.blue= data[HEADER_SIZE+ i* STROKE_HEADER_SIZE+6];
			newVGO->alpha=      (double) data[HEADER_SIZE+ i* STROKE_HEADER_SIZE+7]/100.0;
			
			latestObj=g_list_alloc();
			latestObj =g_list_append(latestObj,newVGO);
			//latestObj->data=newVGO;

			uint16_t max_coord=data[HEADER_SIZE+i*STROKE_HEADER_SIZE+8];
			uint16_t j=0;
			
			//printf("maxCoord %d \n", max_coord);
			
			for(j=0; j< max_coord; j++)
			{	
				   struct coord *newCoord= (struct coord*) malloc(sizeof(struct coord));
				   newCoord->x=data[HEADER_SIZE+ num_strokes* STROKE_HEADER_SIZE+ processed_coords*2];
				   newCoord->y=data[HEADER_SIZE+ num_strokes* STROKE_HEADER_SIZE+ processed_coords*2+1];
				  
				   if((newCoord->x<= WIDTH ||newCoord->y <= HEIGHT) && (newCoord->x!=0 || newCoord->y!=0))
				   {
					   latestObj =g_list_append(latestObj,newCoord);
					   
					 }
					 else
					 {
						// printf("Warning stroke %d, coord: %d (%d,%d)\n",i ,j, newCoord->x, newCoord->y);  

					 }
									
				   //latestObj =g_list_last(latestObj);

				   processed_coords++;
			}


				*history=g_list_append(*history, latestObj);
			
		//printf("obj len: %d\n", g_list_length(latestObj));

	}
	printf("history len: %d, processed coords: %d\n", g_list_length(*history), processed_coords);

}
