#include "morph.h"
#include "math.h"
#include "binary_format.h"

void morph(uint16_t* from, uint16_t* to, uint16_t** target, double_t stage )
{
	uint16_t target_num_strokes=0;
	uint16_t target_coord_size=0;
	uint16_t num_strokes_from=0;
	uint16_t num_strokes_to=0;
	uint16_t num_coords_from=0;
	uint16_t num_coords_to=0;

	num_strokes_from=from[1];
	num_strokes_to=to[1];

		target_num_strokes=linear_interpolation(num_strokes_from, num_strokes_to, stage);
		
		printf("from strokes %d, to strokes %d, target: %d\n", num_strokes_from, num_strokes_to, target_num_strokes);
		

	
	uint16_t* target_stroke_sizes=malloc(sizeof(uint16_t)*target_num_strokes);

	
	int i=0;

	uint16_t value_from=	0;
	uint16_t value_to=	0;
	uint16_t target_value=0;
	uint16_t coord_offset=HEADER_SIZE+target_num_strokes*STROKE_HEADER_SIZE;
	uint16_t coord_offset_from=HEADER_SIZE+num_strokes_from*STROKE_HEADER_SIZE;
	uint16_t coord_offset_to=HEADER_SIZE+num_strokes_to*STROKE_HEADER_SIZE;
	
	uint16_t from_stroke_len=0;
	uint16_t to_stroke_len=0;
	uint16_t target_counter=0;
	uint16_t padding=0;
	uint16_t target_coord_size_temp=0;
	uint16_t from_coord_size=0;
	uint16_t to_coord_size=0;
	
	for(i=0; i< target_num_strokes; i++)
	{
			if(i>num_strokes_from)
			{
				from_stroke_len=0;
				to_stroke_len=to[HEADER_SIZE+STROKE_HEADER_SIZE*i+8];
				to_coord_size+=to_stroke_len;
			}else if(i>num_strokes_to)
			{
				from_stroke_len=from[HEADER_SIZE+STROKE_HEADER_SIZE*i+8];
				to_stroke_len=0;
				from_coord_size+=from_stroke_len;
			}else
			{
				from_stroke_len=from[HEADER_SIZE+STROKE_HEADER_SIZE*i+8];
				to_stroke_len=to[HEADER_SIZE+STROKE_HEADER_SIZE*i+8];
				to_coord_size+=to_stroke_len;
				from_coord_size+=from_stroke_len;
			}
			//target_stroke_sizes[i]=abs((to_stroke_len*1.0-from_stroke_len*1.0)*stage+from_stroke_len);
			target_stroke_sizes[i]=(linear_interpolation((from_stroke_len), (to_stroke_len), stage));
			//printf("from_stroke_len: %d, to_stroke_len %d, target: %d \n", from_stroke_len, to_stroke_len, target_stroke_sizes[i] );
			
			target_coord_size_temp+=target_stroke_sizes[i];
			
		
	}
	

	target_coord_size=target_coord_size_temp;
	printf(" coord size: from %d, to %d target %d\n",from_coord_size, to_coord_size, target_coord_size );

	uint16_t target_size=HEADER_SIZE+STROKE_HEADER_SIZE*target_num_strokes;
	target_size=target_size+target_coord_size*2;
	
	printf("target data_size: %d \n", target_size);
	*target=(uint16_t*)malloc(target_size*sizeof(uint16_t));

	memset(*target,0, target_size);
	//write header
	(*target)[0]=from[0];//seed
	(*target)[1]=target_num_strokes;
	(*target)[2]=HEADER_SIZE+STROKE_HEADER_SIZE*target_num_strokes+target_coord_size*2;
	
		for(i=0; i<= target_num_strokes; i++)
	{
		(*target)[HEADER_SIZE+STROKE_HEADER_SIZE*i+STROKE_HEADER_SIZE-1]=target_stroke_sizes[i];
	}
	

	
	int j=0;
	//morph headers

	for (i=0; i<=target_num_strokes;i++)
	{
		
			for(j=1; j< STROKE_HEADER_SIZE-1;j++)//coord count is set later
			{
				
				
				if(j> num_strokes_from)
				{
					value_from=0;
					if(j==7)value_from=20;//alpha
					if(j==1)value_from=30;//radius
					if(j==2)value_from=1;//fingers
					value_to=to[HEADER_SIZE+STROKE_HEADER_SIZE*i+j];
				}
				
				else if(j> num_strokes_to)
				{
					value_to=0;
					if(j==7)value_to=20;//alpha
					if(j==1)value_to=30;//radius
					if(j==2)value_to=1;//fingers
					value_from=from[HEADER_SIZE+STROKE_HEADER_SIZE*i+j];
				}else
				{
					value_to=to[HEADER_SIZE+STROKE_HEADER_SIZE*i+j];
					value_from=from[HEADER_SIZE+STROKE_HEADER_SIZE*i+j];
				}
				//THIS IS A SHORTTERMFIX
				if(j==2)
				{
					//fingers
				printf("fingers: from %d, to %d\n", value_from, value_to);
					if(value_from>5 || value_to >10)
					{
						target_value=1;
						(*target)[HEADER_SIZE+STROKE_HEADER_SIZE*i+j]=target_value;
					}
					else
					{
						printf("fingers: from %d, to %d\n", value_from, value_to);
						target_value=linear_interpolation(value_from, value_to, stage);
						printf("TARGET: %d from %d\n",target_value, value_from );
						(*target)[HEADER_SIZE+STROKE_HEADER_SIZE*i+j]=target_value;
					};
				
				}
				else
				{
				
				//target_value=abs(((value_to*1.0)-(value_from*1.0))*stage+1.0*value_from);
				target_value=linear_interpolation(value_from, value_to, stage);
				(*target)[HEADER_SIZE+STROKE_HEADER_SIZE*i+j]=target_value;
				}
				
			}
	}
	

	
	//morph content

	uint16_t from_len=0;
	uint16_t to_len=0;
	uint16_t target_len=0;
	
	uint16_t from_offset=coord_offset_from;
	uint16_t to_offset=coord_offset_to;
	uint16_t target_offset=coord_offset;
	

	uint16_t processed_coords=0;
	
	for(i=0; i<target_num_strokes ;i++ )
	{
		target_len=target_stroke_sizes[i];
		if(i> num_strokes_from)
		{
			
			from_len=0;
			to_len=to[HEADER_SIZE+(i+1)*STROKE_HEADER_SIZE-1];
		
		}
		else if(i> num_strokes_to)
		{
			to_len=0;
			from_len=from[HEADER_SIZE+(i+1)*STROKE_HEADER_SIZE-1];
		
		}else 
		{
			to_len=to[HEADER_SIZE+(i+1)*STROKE_HEADER_SIZE-1];
			from_len=from[HEADER_SIZE+(i+1)*STROKE_HEADER_SIZE-1];

		}
		
		processed_coords+=morph_path(from+from_offset, from_len, to+to_offset, to_len, target, target_offset, target_len, stage);

		from_offset=from_offset+from_len*2;
		to_offset=to_offset+to_len*2;
		target_offset=target_offset+target_len*2;
		
		//printf("offsets from %d to %d target %d\n", from_offset, to_offset, target_offset);
		//printf("len from %d to %d target %d\n", from_len, to_len, target_len);
	}
	
	
	printf("processed_coords %d \n",processed_coords);
	
 	printf("offsets from %d to %d target %d\n", from_offset, to_offset, target_offset);
		

printf(" target\n");	
	print_sketch_debug(target, 0);
	printf(" from\n");	
	print_sketch_debug(&from, 0);
	printf(" to\n");	
	print_sketch_debug(&to, 0);

printf(" target\n");	

uint16_t last10zero=0;
for(i=0; i< target_size; i++)
{
	//printf(" %d ", (*target)[i]);
	
	if((*target)[i]==0)
	{
		last10zero++;
	
	}else
	{
		last10zero=0;
	}
	if(last10zero==10)
	{
	printf("### %d\n", i);
	
	break	;
	}
}
printf("\n");

	printf("from \n\n\n");
		
for(i=0; i< from[2]; i++)
{
	//printf(" %d ", from[i]);	
}
printf("i:%d \n",i );
/*
printf(" to\n\n\n");	
	
for(i=0; i< to[2]; i++)
{
	printf(" %d ", to[i]);	
}
printf("\n");
*/

	

free (target_stroke_sizes);
}


uint16_t morph_path(uint16_t* from, uint16_t from_len, uint16_t* to, uint16_t to_len, uint16_t** target, uint16_t target_offset, uint16_t target_len, double stage)
{
	uint16_t delta_to_from=0;
	uint16_t delta_to_max=0;
	uint16_t* shorter_path=NULL;
	uint16_t shorter_len=0;
	uint16_t* target_p=(*target)+target_offset;
	
	uint16_t* longer_path=NULL;
	uint16_t longer_len=0;
	uint16_t last=0;
	
	double stage_to_len=0.0;
	
	if(to_len < from_len)
	{
		shorter_path=to;
		shorter_len=to_len;
		
		longer_path=from;
		longer_len=from_len;
		stage_to_len=1.0-stage;
	
	}
	else
	{
		shorter_path=from;
		shorter_len=from_len;
		
		longer_path=to;
		longer_len=to_len;
		stage_to_len=stage;
		
	}
	delta_to_from=longer_len-shorter_len;
	delta_to_max=longer_len-target_len;
	int i=0;
	
	for(i=0; i< target_len; i++)
	{
		if(i < shorter_len)
		{
			last=i;
			uint16_t result_x=0;
			uint16_t result_y=0;
			
			linear_interpolation_pair(shorter_path[i*2],shorter_path[i*2+1],  longer_path[i*2], longer_path[i*2+1],
										stage_to_len,
										&result_x, &result_y );
			target_p[i*2]=result_x;
			target_p[i*2+1]=result_y;
		   
		   if((result_x> WIDTH ||result_y > HEIGHT) || (result_x==0 || result_y==0))
		   {
				printf("shady coordinate %i: %d, %d \n",i, result_x, result_y);
		   }
		
		}
		else
		{
			uint16_t delta_to_shorter= i-shorter_len;
			uint16_t result_x=0;
			uint16_t result_y=0;
			shrink_interpolation_pair(shorter_path[last*2], shorter_path[last*2+1],  longer_path[i*2], longer_path[i*2+1],
										delta_to_shorter, delta_to_from, stage_to_len,
										&result_x, &result_y );
			target_p[i*2]=result_x;
			target_p[i*2+1]=result_y;
			
		   if((result_x> WIDTH ||result_y > HEIGHT) || (result_x==0 || result_y==0))
		   {
				printf("shady coordinate: %d, %d \n",i, result_x, result_y);
		   }
			
		}
		//printf("[%d/%d] (%d, %d) -> target (%d,%d) \n",i, target_len, longer_path[i], longer_path[i+1], target_p[i], target_p[i+1]);
	}
	
	
	return i;
}		
// interpolation viewed as from shorter to longer
uint16_t linear_interpolation(uint16_t shorter_value,  uint16_t longer_value, double stage_to_len)
{
	uint16_t result=0;
	//(1 - t) * v0 + t * v1;
	result= (1.0-stage_to_len)*(shorter_value*1.0)+(stage_to_len*longer_value);
	//result=abs((longer*1.0-shorter*1.0)*stage_to_len+shorter*1.0);
	//printf("lerp: %u\n",result );
	return result;
}

//interpolation between last value of shortest path and new values

uint16_t shrink_interpolation(uint16_t shorter_value, uint16_t  longer_value, uint16_t current_delta_to_shorter, uint16_t delta_to_shorter, double stage_to_len)
{
	uint16_t result=0;
	double weighted_stage=stage_to_len;//*((current_delta_to_shorter*1.0)/(delta_to_shorter*1.0));
	result= (1.0-stage_to_len)*(shorter_value*1.0)+(stage_to_len*longer_value);
	//printf("lerp_: %u\n",result );
	return result;
}


// interpolation viewed as from shorter to longer
void linear_interpolation_pair(uint16_t shorter_value_x, uint16_t shorter_value_y,

	  uint16_t longer_value_x, uint16_t longer_value_y,
	  
	  double stage_to_len, uint16_t* return_x, uint16_t* return_y)
{
	uint16_t result_x=0;
	uint16_t result_y=0;
	//(1 - t) * v0 + t * v1;
	result_x= (1.0-stage_to_len)*(shorter_value_x*1.0)+(stage_to_len*longer_value_x);
	result_y= (1.0-stage_to_len)*(shorter_value_y*1.0)+(stage_to_len*longer_value_y);
	//result=abs((longer*1.0-shorter*1.0)*stage_to_len+shorter*1.0);
	//printf("lerp: %u\n",result );
	if(result_x>WIDTH)result_x=0;
	if(result_y>HEIGHT)result_y=0;
	*return_x=result_x;
	*return_y=result_y;
	//printf("lerp_ (stage2len %f): %u %u longer( %d, %d)\n", stage_to_len ,result_x, result_y, longer_value_x,longer_value_y );
	return ;
}

//interpolation between last value of shortest path and new values

void shrink_interpolation_pair(uint16_t shorter_value_x, uint16_t shorter_value_y,

	 uint16_t  longer_value_x, uint16_t  longer_value_y,
	 
	 uint16_t current_delta_to_shorter, uint16_t delta_to_shorter, double stage_to_len, uint16_t* return_x, uint16_t* return_y)
{
	uint16_t result_x=0;
	uint16_t result_y=0;
	double weighted_stage=stage_to_len;//*((current_delta_to_shorter*1.0)/(delta_to_shorter*1.0));
	result_x= (1.0-stage_to_len)*(shorter_value_x*1.0)+(stage_to_len*longer_value_x);
	result_y= (1.0-stage_to_len)*(shorter_value_y*1.0)+(stage_to_len*longer_value_y);
	//printf("lerp_: %u\n",result );
	if(result_x>WIDTH)result_x=0;
	if(result_y>HEIGHT)result_y=0;
	*return_x=result_x;
	*return_y=result_y;
		//printf("lerp_s (stage2len %f): %u %u longer( %d, %d)\n", stage_to_len ,result_x, result_y, longer_value_x,longer_value_y );

	return ;
}

